---
title: Premium Coding Bootcamp
description: Recommendation for the inception of a premium bootcamp product 
author: Orwa M. Diraneyya (@odiraneyya)
keywords: marp,marp-cli,slide
url: 
marp: true
image: ./assets/logo.png
---

<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>


![](./assets/logo.png){.float-right}
# Premium Coding Bootcamp | Recommendations
## by Orwa Diraneyya

---

# ORGANIZATION

- The bad
- The good
- The lesson
- The dilemma
- The recommendations

---

# THE BAD (1)

## Introduction

Many of the good programmers who went through a an academic degree in CS see coding Bootcamps as scams.

The reason for this is simple. It is near impossible for some people to learn coding in a few months, and become good enough at it to be paid to do it full-time.

> TLDR; _some people cannot code_

---

# THE BAD (2)

## Business Model (1)

Because "making people code" or "teaching them how to code" is notoriously difficult in some cases, coding Bootcamps usually aim for a more-attainable goal.

The goal is usually: "make it seem as if someone knows how to code" (regardless of whether they do or not) by spoon feeding them projects, and help them populate their LinkedIn and GitHub accounts

---

# THE BAD (3)

## Business Model (2)

... This way, a recruiter, once looking at these would be tempted to think that the student is employable.

This harsh truth is discovered rather quickly, during a short technical interview, but more often, using an automated coding test gate that rejects the candidate before even entering the interviewing process.

> TLDR; _some coding Bootcamps are scams_

---

# THE BAD (4)

# Bootcamp-Quality Developers

But even when the students can code, they are often inferior to their academic counterparts.

This is due to significant knowledge gaps in:
1. How computers operate at a low level, including memory and device access
1. The lower level concepts underlying numbering systems and data representation
1. A general deficiency in mathematics and logic

> TLDR; _bootcamps-taught developers are unsafe bets for high-end employers_

---

# THE GOOD

The bad is also a good thing, because it creates room for improvement.

And despite all the previous issues with existing Bootcamps, the "Bootcamp approach" to teaching coding remains, in my opinion, more modern and more effective than the academic route.

---

# THE LESSON

The most important conclusion to be made at this point is that:

> Making someone who cannot code seem as if they can code (as some coding Bootcamps do) delivers no value to the student(s) or the employer(s)

Hence, the strategy should not be to make it seem as if someone who cannot code can code, but rather, to ensure that everyone who graduates from the coding Bootcamp **can actually code.**

> TLDR; _A premium bootcamp should not allow people who cannot code graduate_

---

# THE DILEMMA

The dilemma is now clear.

> Some people cannot code, and we have to deliver the service of graduation to our students, but we cannot let someone who cannot code graduate. 🤯

**How to solve this dilemma:**
1. Eliminate _those who cannot code_ using a stricter admission process `STRATEGY-1`
1. Maximize the likelihood that _someone can code_, which requires an understanding of _why some people cannot code_ `STRATEGY-2`
1. Let people go, along with their funding, which is a last resort but is a necessary measure in a premium bootcamp `STRATEGY-3`

---

# `STRATEGY-1`

Despite the appeal of eliminating people through admission, this has obvious drawback that rejecting someone from the limited information available in the admission phase unnecessarily limits our customer base.

In simpler terms, the stricter the admission process is, the more limited our customer pool is, and the lower of a glass ceiling we have on our yearly earnings.

Hence, this works against growth in sales.


---

# `STRATEGY-1`: RECOMMENDATION

My recommendation in this area is to adopt an established form of an "intelligence test", rather than trying to come up with our own (unproven) tests.

The idea is to asses the candidates logical inference and generalization abilities, which either makes it possible or impossible understand and re-apply coding concepts instead of using memorization.

_This can be completely outsourced, and hence should be budgeted for._

---

# `STRATEGY-2`

As mentioned [here](https://www.linkedin.com/pulse/why-do-coding-seems-just-impossible-some-us-orwa-diraneyya/) I believe that difficulties in learning how to code stem from:
- Past work/life adaptations
- Circumstantial reasons (not mentioned in the article)

Past adaptations are apparent in communication professionals, house wives, coaches, and other members of society. Those adaptations make the coding learning curve much steeper and hence it would take much longer for those people to learn how to code.

Circumstantial reasons include the general lack of autonomy (apparent in housewives and young adults) as well as limited resources (laptops, space, etc.).

---

# `STRATEGY-2`: RECOMMENDATION

## Past adaptations

To counteract past work/life adaptations (explained in my LinkedIn article), we must understand that these people can eventually code, but it takes them much much longer to learn the skill than the rest of the people.

Hence, and in this case, the optimal solution would be an _autonomous prep course_ that does not attempt to teach coding but rather focuses on the principles of logical languages (recommendations can be found [here](https://github.com/orwa84/prep-course-recommendation#21-ideas-for-prep-course-content)).

_This course can also be outsourced, and should be budgeted for._

---

# `STRATEGY-2`: EXPLANATION

## Circumstantial (1)

There are situations where, a person's lack of autonomy might hinder severely their ability to learn anything that is theoretical and non-practical.

In effect, losing autonomy activates the survival part of the brain, also called the primal brain. This part of our brains cannot comprehend higher-level concepts.

To be able to activate the modern brain, one needs to feel safe, and re-gain their autonomy.

In the next slide, I mention some of the ways in which this can be done.

---

# `STRATEGY-2`: RECOMMENDATION

## Circumstantial (2)

... This includes giving remote students the option, and sometimes requiring them to, go to professional workspaces (i.e. WeWork) _where they have the right atmosphere for learning. This needs to be budgeted for._

Sometimes however, a new "clean" environment is not sufficient to overcome certain personal issues, habits and patterns. In this case, I would argue that _a resident counsellor for the program might be of great help, which needs to be hired and budgeted for._

---

# `STRATEGY-3`: RECOMMENDATION

Lastly, there will be cases where students need to be let go, along with their funding.

This is not only due to their inability to code, but also due to behavioral, and other reasons that counteract discipline, and create an unsafe environment in the classroom.

For example, a student who is unavailable despite many warning.

Dismissing such student _should be an option for the school that can be justified in business terms, using the objective of wanting to create value, and hence should be budgeted for._